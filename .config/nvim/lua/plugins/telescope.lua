return {
  'nvim-telescope/telescope.nvim',
  branch = '0.1.x',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'nvim-telescope/telescope-project.nvim',
    -- Fuzzy Finder Algorithm which requires local dependencies to be built.
    -- Only load if `make` is available. Make sure you have the system
    -- requirements installed.
    {
      'nvim-telescope/telescope-fzf-native.nvim',
      -- NOTE: If you are having trouble with this installation,
      --       refer to the README for telescope-fzf-native for more instructions.
      build = 'make',
      cond = function()
        return vim.fn.executable 'make' == 1
      end,
    },
  },
  config = function()
    -- Implement delta as previewer for diffs

    local previewers = require('telescope.previewers')
    local builtin = require('telescope.builtin')
    local conf = require('telescope.config')
    local M = {}

    local delta = previewers.new_termopen_previewer {
      get_command = function(entry)
        -- this is for status
        -- You can get the AM things in entry.status. So we are displaying file if entry.status == '??' or 'A '
        -- just do an if and return a different command
        if entry.status == '??' or 'A ' then
          return { 'git', '-c', 'core.pager=delta', '-c', 'delta.side-by-side=false', 'diff', entry.value }
        end

        -- note we can't use pipes
        -- this command is for git_commits and git_bcommits
        return { 'git', '-c', 'core.pager=delta', '-c', 'delta.side-by-side=false', 'diff', entry.value .. '^!' }

      end
    }

    M.my_git_commits = function(opts)
      opts = opts or {}
      opts.previewer = delta

      builtin.git_commits(opts)
    end

    M.my_git_bcommits = function(opts)
      opts = opts or {}
      opts.previewer = delta

      builtin.git_bcommits(opts)
    end

    M.my_git_status = function(opts)
      opts = opts or {}
      opts.previewer = delta

      builtin.git_status(opts)
    end

    M.project_files = function()
      local opts = {} -- define here if you want to define something
      local ok = pcall(require 'telescope.builtin'.git_files, opts)
      if not ok then require 'telescope.builtin'.find_files(opts) end
    end

    return M
  end
}
