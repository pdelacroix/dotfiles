return {
  -- LSP Configuration & Plugins
  'neovim/nvim-lspconfig',
  dependencies = {
    -- Automatically install LSPs to stdpath for neovim
    -- 'williamboman/mason.nvim',
    -- 'williamboman/mason-lspconfig.nvim',

    -- Useful status updates for LSP
    -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
    { 'j-hui/fidget.nvim', tag = 'legacy', opts = {} },

    -- Additional lua configuration, makes nvim stuff amazing!
    'folke/neodev.nvim',
  },
  config = function()
    local lsp = require "lspconfig"
    local coq = require "coq"

    local capabilities = vim.lsp.protocol.make_client_capabilities()
    capabilities.textDocument.completion.completionItem.snippetSupport = true

    -- lsp.ocamllsp.setup(coq.lsp_ensure_capabilities { cmd = { "esy", "ocamllsp", "--fallback-read-dot-merlin"} })
    -- lsp.ocamllsp.setup(coq.lsp_ensure_capabilities { cmd = { "ocamllsp", "--fallback-read-dot-merlin"} })
    lsp.ocamllsp.setup(coq.lsp_ensure_capabilities { cmd = { "opam", "exec", "--", "ocamllsp"}, filetypes = { "ocaml", "ocaml.menhir", "ocaml.interface", "ocaml.ocamllex", "reason", "dune", "rescript" } })
    -- lsp.ocamllsp.setup(coq.lsp_ensure_capabilities { cmd = { "dune", "exec", "ocamllsp"}, filetypes = { "ocaml", "ocaml.menhir", "ocaml.interface", "ocaml.ocamllex", "reason", "dune", "rescript" } })

    -- lsp.rescriptls.setup(coq.lsp_ensure_capabilities {
    --   capabilities = capabilities,
    --   cmd = {
    --   'node',
    --   '/home/pierre/.local/share/nvim/site/pack/packer/start/vim-rescript/server/out/server.js',
    --   '--stdio'
    -- }
    -- })

    lsp.elixirls.setup(coq.lsp_ensure_capabilities {
        cmd = { "/home/pierre/.local/share/elixir-ls/release/language_server.sh" };
      })

    lsp.ts_ls.setup{}

    lsp.solargraph.setup(coq.lsp_ensure_capabilities {
        capabilities = capabilities,
      })

    lsp.cssls.setup(coq.lsp_ensure_capabilities {
        capabilities = capabilities,
      })

    lsp.dockerls.setup(coq.lsp_ensure_capabilities {})

    lsp.jsonls.setup(coq.lsp_ensure_capabilities {
        commands = {
          Format = {
            function()
              vim.lsp.buf.range_formatting({}, { 0, 0 }, { vim.fn.line("$"), 0 })
            end
          }
        }
      })

    lsp.yamlls.setup(coq.lsp_ensure_capabilities {})

    lsp.vimls.setup(coq.lsp_ensure_capabilities {})

    require'lspconfig'.lua_ls.setup {
      settings = {
        Lua = {
          runtime = {
            -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
            version = 'LuaJIT',
          },
          diagnostics = {
            -- Get the language server to recognize the `vim` global
            globals = {'vim'},
          },
          workspace = {
            -- Make the server aware of Neovim runtime files
            library = vim.api.nvim_get_runtime_file("", true),
          },
          -- Do not send telemetry data containing a randomized but unique identifier
          telemetry = {
            enable = false,
          },
        },
      },
    }

    require'lspconfig'.phpactor.setup{}
  end
}
