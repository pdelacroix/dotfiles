-- local execute = vim.api.nvim_command
-- local fn = vim.fn
-- 
-- local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
-- 
-- if fn.empty(fn.glob(install_path)) > 0 then
--   fn.system({ 'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path })
--   execute 'packadd packer.nvim'
-- end
-- 
-- -- vim.cmd 'autocmd BufWritePost plugins.lua PackerCompile' -- Auto compile when there are changes in plugins.lua
-- local packer_group = vim.api.nvim_create_augroup('Packer', { clear = true })
-- vim.api.nvim_create_autocmd('BufWritePost',
--   { command = 'source <afile> | PackerCompile', group = packer_group, pattern = 'plugins.lua' })
-- 
-- -- require('packer').init({display = {non_interactive = true}})
-- require('packer').init({ display = { auto_clean = false } })
-- 
-- return require('packer').startup(function(use)
--   -- Packer can manage itself as an optional plugin
--   use 'wbthomason/packer.nvim'
-- 
--   use 'nvim-lua/popup.nvim'
--   use 'nvim-lua/plenary.nvim'
-- 
--   use 'famiu/feline.nvim'
-- 
--   use { 'stevearc/dressing.nvim' }
-- 
--   use({ 'mrjones2014/legendary.nvim' })
-- 
--   use 'dstein64/vim-startuptime'
-- 
--   -- use {
--   --   'kyazdani42/nvim-tree.lua',
--   --   requires = {
--   --     'kyazdani42/nvim-web-devicons', -- optional, for file icon
--   --   },
--   --   config = function() require 'nvim-tree'.setup {} end
--   -- }
--   use {
--     "nvim-neo-tree/neo-tree.nvim",
--     branch = "v2.x",
--     requires = {
--       "nvim-lua/plenary.nvim",
--       "kyazdani42/nvim-web-devicons", -- not strictly required, but recommended
--       "MunifTanjim/nui.nvim",
--       {
--         -- only needed if you want to use the commands with "_with_window_picker" suffix
--         's1n7ax/nvim-window-picker',
--         tag = "v1.*",
--         config = function()
--           require 'window-picker'.setup({
--             autoselect_one = true,
--             include_current = false,
--             filter_rules = {
--               -- filter using buffer options
--               bo = {
--                 -- if the file type is one of following, the window will be ignored
--                 filetype = { 'neo-tree', "neo-tree-popup", "notify", "quickfix", "trouble" },
-- 
--                 -- if the buffer type is one of following, the window will be ignored
--                 buftype = { 'terminal' },
--               },
--             },
--             other_win_hl_color = '#e35e4f',
--           })
--         end,
--       }
--     }
--   }
-- 
--   use {
--     "mrbjarksen/neo-tree-diagnostics.nvim",
--     requires = "nvim-neo-tree/neo-tree.nvim",
--     -- module = "neo-tree.sources.diagnostics", -- if wanting to lazyload
--   }
-- 
--   use {
--     'TimUntersberger/neogit',
--     requires = {
--       'nvim-lua/plenary.nvim', -- optional, for file icon
--       'sindrets/diffview.nvim'
--     },
--     config = function() require 'neogit'.setup {
--         integrations = {
--           diffview = true
--         },
--       }
--     end
--   }
-- 
--   use { 'sindrets/diffview.nvim', requires = 'nvim-lua/plenary.nvim' }
-- 
--   use 'fneu/breezy'
--   -- use 'glepnir/zephyr-nvim'
--   -- use "rebelot/kanagawa.nvim"
--   use { "catppuccin/nvim", as = "catppuccin", run = ":CatppuccinCompile" }
-- 
--   use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate',
--     requires = { 'nvim-treesitter/nvim-treesitter-textobjects', 'folke/which-key.nvim' }, }
--   use 'nvim-treesitter/playground'
-- 
--   use 'neovim/nvim-lspconfig'
--   -- use 'hrsh7th/nvim-compe'
--   use { 'ms-jpq/coq_nvim', branch = 'coq' }
--   use { 'ms-jpq/coq.artifacts', branch = 'artifacts' }
--   use { 'ms-jpq/coq.thirdparty', branch = '3p' }
-- 
--   use 'nvim-telescope/telescope.nvim'
--   use 'nvim-telescope/telescope-fzy-native.nvim'
--   use 'nvim-telescope/telescope-project.nvim'
-- 
--   use { 'lewis6991/gitsigns.nvim', requires = { 'nvim-lua/plenary.nvim' } }
-- 
--   use 'christoomey/vim-tmux-navigator'
-- 
--   -- use 'ggandor/lightspeed.nvim'
--   use {
--     'numToStr/Comment.nvim',
--     config = function()
--       require('Comment').setup()
--     end
--   }
-- 
--   use 'sheerun/vim-polyglot'
--   use 'elixir-editors/vim-elixir'
-- 
--   use {
--     "folke/which-key.nvim",
--     config = function()
--       require("which-key").setup {
--       }
--     end
--   }
-- 
--   use {
--     "ggandor/leap.nvim",
--     config = function()
--       require('leap').set_default_keymaps()
--     end
--   }
-- 
--   use {
--     "folke/trouble.nvim",
--     requires = "kyazdani42/nvim-web-devicons",
--     config = function()
--       require("trouble").setup {
--         -- your configuration comes here
--         -- or leave it empty to use the default settings
--         -- refer to the configuration section below
--       }
--     end
--   }
-- 
--   use 'rescript-lang/vim-rescript'
-- end)

-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- Install package manager
--    https://github.com/folke/lazy.nvim
--    `:help lazy.nvim.txt` for more info
local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system {
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  }
end
vim.opt.rtp:prepend(lazypath)

-- NOTE: Here is where you install your plugins.
--  You can configure plugins using the `config` key.
--
--  You can also configure plugins after the setup call,
--    as they will be available in your neovim runtime.
require('lazy').setup({
  -- NOTE: First, some plugins that don't require any configuration

  -- Git related plugins
  -- 'tpope/vim-fugitive',
  -- 'tpope/vim-rhubarb',

  'nvim-lua/popup.nvim',

  'stevearc/dressing.nvim',

  'mrjones2014/legendary.nvim',

  'dstein64/vim-startuptime',

  {
    "mrbjarksen/neo-tree-diagnostics.nvim",
    dependencies = {"nvim-neo-tree/neo-tree.nvim"},
  },

  {
    "NeogitOrg/neogit",
    dependencies = {
      "nvim-lua/plenary.nvim",         -- required
      "nvim-telescope/telescope.nvim", -- optional
      "sindrets/diffview.nvim",        -- optional
      "ibhagwan/fzf-lua",              -- optional
    },
    -- config = true,
    opts = {
        integrations = {
          diffview = true
        },
      }
  },

  'fneu/breezy',
  -- use 'glepnir/zephyr-nvim'
  -- use "rebelot/kanagawa.nvim"

  { 'ms-jpq/coq_nvim', branch = 'coq' },
  { 'ms-jpq/coq.artifacts', branch = 'artifacts' },
  { 'ms-jpq/coq.thirdparty', branch = '3p' },


  'christoomey/vim-tmux-navigator',

  -- use 'ggandor/lightspeed.nvim'
  {
    'numToStr/Comment.nvim',
    config = true
  },

  'sheerun/vim-polyglot',
  'elixir-editors/vim-elixir',

  {
    "folke/which-key.nvim",
    config = true
  },

  {
    "ggandor/leap.nvim",
    config = function()
      require('leap').set_default_keymaps()
    end
  },

  {
    "folke/trouble.nvim",
    dependencies = {"nvim-tree/nvim-web-devicons"},
    config = true
  },

  'rescript-lang/vim-rescript',

  -- Detect tabstop and shiftwidth automatically
  -- 'tpope/vim-sleuth',

  -- {
  --   -- Set lualine as statusline
  --   'nvim-lualine/lualine.nvim',
  --   -- See `:help lualine.txt`
  --   opts = {
  --     options = {
  --       icons_enabled = false,
  --       theme = 'onedark',
  --       component_separators = '|',
  --       section_separators = '',
  --     },
  --   },
  -- },

  -- {
  --   -- Add indentation guides even on blank lines
  --   'lukas-reineke/indent-blankline.nvim',
  --   -- Enable `lukas-reineke/indent-blankline.nvim`
  --   -- See `:help indent_blankline.txt`
  --   main = 'ibl',
  --   opts = {},
  -- },

  -- NOTE: Next Step on Your Neovim Journey: Add/Configure additional "plugins" for kickstart
  --       These are some example plugins that I've included in the kickstart repository.
  --       Uncomment any of the lines below to enable them.
  -- require 'kickstart.plugins.autoformat',
  -- require 'kickstart.plugins.debug',

  -- NOTE: The import below can automatically add your own plugins, configuration, etc from `lua/custom/plugins/*.lua`
  --    You can use this folder to prevent any conflicts with this init.lua if you're interested in keeping
  --    up-to-date with whatever is in the kickstart repo.
  --    Uncomment the following line and add your plugins to `lua/custom/plugins/*.lua` to get going.
  --
  --    For additional information see: https://github.com/folke/lazy.nvim#-structuring-your-plugins
  { import = 'plugins' },
})
