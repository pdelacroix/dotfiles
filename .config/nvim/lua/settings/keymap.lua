--Remap space as leader key
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

--Remap for dealing with word wrap
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

require('legendary').setup()

local wk = require("which-key")

wk.add({
  -- ['[c'] = { 'Go to previous chunk' },
  -- [']c'] = { 'Go to next chunk' },
  -- ['[d'] = { ':lua vim.diagnostic.goto_prev()<CR>', 'Go to previous diagnostic' },
  -- [']d'] = { ':lua vim.diagnostic.goto_next()<CR>', 'Go to next diagnostic' },
  -- ["[c"] = { function()
  --   require('gitsigns').setqflist("all", { open = false })
  --   require("trouble").open({mode = "quickfix", auto = true})
  --   require("trouble").refresh()
  --   require("trouble").previous({ skip_groups = true, jump = true })
  -- end, "Jump to previous quickfix" },
  -- ["]c"] = { function()
  --   require('gitsigns').setqflist("all", { open = false })
  --   require("trouble").open({mode = "quickfix", auto = true})
  --   require("trouble").refresh()
  --   require("trouble").next({ skip_groups = true, jump = true })
  -- end, "Jump to previous quickfix" },
  {
    "[[",
    function() for i = 1, vim.v.count1 do vim.api.nvim_command("TSTextobjectGotoPreviousStart @function.outer") end end,
    desc = "Jump to previous function"
  },
  {
    "]]",
    function() for i = 1, vim.v.count1 do vim.api.nvim_command("TSTextobjectGotoNextStart @function.outer") end end,
    desc = "Jump to next function"
  },
  {
    "[d",
    function()
      require("trouble").open({ mode = "workspace_diagnostics", auto = true })
      require("trouble").refresh()
      require("trouble").previous({ skip_groups = true, jump = true })
    end,
    desc = "Jump to previous quickfix"
  },
  {
    "]d",
    function()
      require("trouble").open({ mode = "workspace_diagnostics", auto = true })
      require("trouble").refresh()
      require("trouble").next({ skip_groups = true, jump = true })
    end,
    desc = "Jump to previous quickfix"
  },
  {
    "[q",
    function()
      if vim.fn.bufwinid("Trouble") == -1 then
        vim.api.nvim_exec("cprev", false)
      else
        require("trouble").previous({ skip_groups = true, jump = true })
      end
    end,
    desc = "Jump to previous quickfix"
  },
  {
    "]q",
    function()
      if vim.fn.bufwinid("Trouble") == -1 then
        vim.api.nvim_exec("cnext", false)
      else
        require("trouble").next({ skip_groups = true, jump = true })
      end
    end,
    desc = "Jump to next quickfix"
  },
  { "<C-1>",  ":Neotree toggle<CR>",                                         desc = "Toggle tree" },
  { "<C-\\>", ":TmuxNavigatePrevious<CR>",                                   desc = "Move to previous split" },
  { "<C-h>",  ":TmuxNavigateLeft<CR>",                                       desc = "Move to left split" },
  { "<C-j>",  ":TmuxNavigateDown<CR>",                                       desc = "Move to bottom split" },
  { "<C-k>",  ":TmuxNavigateUp<CR>",                                         desc = "Move to top split" },
  { "<C-l>",  ":TmuxNavigateRight<CR>",                                      desc = "Move to right split" },
  { "<C-n>",  ":Neotree reveal<CR>",                                         desc = "Show current file in tree" },
  { "<C-p>",  desc = "Preview hunk" },
  { "<C-t>",  ":Neotree focus<CR>",                                          desc = "Focus tree" },
  { "K",      ":lua vim.lsp.buf.hover()<CR>",                                desc = "LSP hover" },
  { "gD",     ":lua require('telescope.builtin').lsp_implementations()<CR>", desc = "LSP find implementations" },
  { "gR",     "<cmd>Trouble lsp_references<cr>",                             desc = "LSP references in Trouble" },
  { "gd",     ":lua vim.lsp.buf.definition()<CR>",                           desc = "LSP go to definition" },
})

wk.add({
  -- ['c'] = { ":lua require('plugins.telescope').my_git_commits()<CR>", 'Show commits' },
  -- f = { "<cmd>Telescope find_files<cr>", "Find File" }, -- create a binding with label
  -- r = { "<cmd>Telescope oldfiles<cr>", "Open Recent File", noremap=false, buffer = 123 }, -- additional options for creating the keymap
  -- n = { "New File" }, -- just a label. don't create any mapping
  -- e = "Edit File", -- same as above
  -- ["1"] = "which_key_ignore",  -- special label to hide it in the popup
  -- b = { function() print("bar") end, "Foobar" } -- you can also pass functions!
  -- ['g'] = { ":lua require('plugins.telescope').my_git_status()<CR>", 'Show Git status' },
  -- ['G'] = { ":lua require('plugins.telescope').my_git_bcommits()<CR>", 'Show bcommits' },
  -- t = {
  --   name = "tree",
  --   t = { ':NvimTreeToggle<CR>', 'Toggle tree' },
  --   r = { ':NvimTreeRefresh<CR>', 'Refresh tree' },
  -- },
  -- ['L'] = { ":lua require('legendary').find('commands')<CR>", 'Search commands' },
  -- ['L'] = { ":lua require('legendary').find('autocmds')<CR>", 'Search autocmds' },
  { "<leader>/",   ":lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>", desc = "Search in buffer" },
  { "<leader>?",   ":lua require('telescope.builtin').live_grep()<CR>",                 desc = "Live grep" },
  { "<leader>a",   ":lua require('telescope.builtin').lsp_code_actions()<CR>",          desc = "LSP code actions" },
  { "<leader>b",   ":lua require('telescope.builtin').buffers()<CR>",                   desc = "List buffers" },
  { "<leader>d",   ":lua require('telescope.builtin').lsp_document_diagnostics()<CR>",  desc = "LSP document diagnostics" },
  { "<leader>D",   ":lua require('telescope.builtin').lsp_workspace_diagnostics()<CR>", desc = "LSP workspace diagnostics" },
  { "<leader>e",   ":lua vim.diagnostic.open_float()<CR>",                              desc = "Show diagnostic" },
  { "<leader>g",   ":Neogit<CR>",                                                       desc = "Neogit" },
  { "<leader>R",   ":lua require('telescope.builtin').grep_string()<CR>",               desc = "Search for string" },
  { "<leader>S",   ":lua require('telescope.builtin').lsp_workspace_symbols()<CR>",     desc = "LSP workspace symbols" },
  { "<leader>V",   ":DiffviewOpen origin/main...HEAD<CR>",                              desc = "Diffview branch / origin/main" },
  { "<leader>r",   ":lua require('telescope.builtin').lsp_references()<CR>",            desc = "LSP references" },
  { "<leader>s",   ":lua require('telescope.builtin').lsp_document_symbols()<CR>",      desc = "LSP document symbols" },
  { "<leader>v",   ":DiffviewOpen -uno<CR>",                                            desc = "Diffview index" },
  { "<leader>H",   ":DiffviewFileHistory<CR>",                                          desc = "Diffview file history" },
  { "<leader>L",   ":lua require('legendary').find()<CR>",                              desc = "Search keymaps, commands, and autocmds" },
  { "<leader>l",   ":lua require('legendary').find('keymaps')<CR>",                     desc = "Search keymaps" },
  { "<leader>m",   ":lua vim.lsp.buf.rename()<CR>",                                     desc = "LSP rename" },
  { "<leader>o",   ":lua vim.lsp.buf.format { async = true }<CR>",                      desc = "LSP format" },

  { "<leader>f",   group = "find" },
  { "<leader>ff",  ":lua require('plugins.telescope').project_files()<CR>",             desc = "Find project file" },
  { "<leader>fh",  ":lua require('telescope.builtin').help_tags()<CR>",                 desc = "Find help tags" },
  { "<leader>fk",  ":lua require('telescope.builtin').keymaps()<CR>",                   desc = "Find keymap" },
  { "<leader>fm",  ":lua require('telescope.builtin').marks()<CR>",                     desc = "Find mark" },
  { "<leader>fq",  ":lua require('telescope.builtin').quickfix()<CR>",                  desc = "Find Quickfix" },
  { "<leader>fr",  ":lua require('telescope.builtin').registers()<CR>",                 desc = "Find register" },
  { "<leader>fs",  ":lua require('telescope.builtin').treesitter()<CR>",                desc = "Find TS command" },
  { "<leader>ft",  ":lua require('telescope.builtin').tags()<CR>",                      desc = "Find tags" },

  { "<leader>F",   group = "trouble" },
  { "<leader>Fd",  "<cmd>Trouble lsp_document_diagnostics<cr>",                         desc = "LSP document diagnostics in Trouble" },
  { "<leader>Fl",  "<cmd>Trouble loclist<cr>",                                          desc = "Loclist in Trouble" },
  { "<leader>Fq",  "<cmd>Trouble quickfix<cr>",                                         desc = "Quickfix in Trouble" },
  { "<leader>Fw",  "<cmd>Trouble lsp_workspace_diagnostics<cr>",                        desc = "LSP workspace diagnostics in Trouble" },
  { "<leader>Fx",  "<cmd>Trouble<cr>",                                                  desc = "Toggle Trouble" },

  { "<leader>h",   group = "Hunks" },
  { "<leader>hD",  desc = "Diff this ~" },
  { "<leader>hR",  desc = "Reset buffer" },
  { "<leader>hS",  desc = "Stage buffer" },
  { "<leader>hb",  desc = "Blame line" },
  { "<leader>hd",  desc = "Diff this" },
  { "<leader>hp",  desc = "Preview hunk" },
  { "<leader>hr",  desc = "Reset hunk" },
  { "<leader>hs",  desc = "Stage hunk" },
  { "<leader>ht",  group = "Toggle" },
  { "<leader>htb", desc = "Current line blame" },
  { "<leader>htd", desc = "Deleted" },
  { "<leader>hu",  desc = "Undo stage hunk" },

  { "<leader>q",   group = "Quickfix list" },
  { "<leader>qq",  ":TroubleToggle quickfix<CR>",                                       desc = "Trouble quickfix list" },
  {
    "<leader>qd",
    function()
      vim.diagnostic.setqflist({ open = false })
      -- vim.cmd 'autocmd DiagnosticChanged * lua vim.diagnostic.setqflist({open = false })'
      vim.api.nvim_create_autocmd("DiagnosticChanged", {
        pattern = "*",
        callback = function()
          vim.diagnostic.setqflist({ open = false })
          require 'trouble'.refresh({ auto = true, provider = "quickfix" })
        end
      })
      require('trouble').setup({ mode = "quickfix" })
    end,
    desc = 'Populate with diagnostics'
  },
  {
    "<leader>qc",
    function()
      vim.api.nvim_clear_autocmds({ event = "DiagnosticChanged" })
      -- vim.cmd 'Gitsigns setqflist'
      require('gitsigns').setqflist("all", { open = false })
      require('trouble').setup({ mode = "quickfix" })
    end,
    desc = 'Populate with chunks'
  },
})

wk.add({
  { "zA", desc = "Toggle fold recursively" },
  { "zC", desc = "Close all folds here" },
  { "zM", desc = "Close all folds" },
  { "zO", desc = "Open all folds here" },
  { "zR", desc = "Open all folds" },
  { "zX", desc = "Update folds" },
  { "za", desc = "Toggle fold" },
  { "zc", desc = "Close fold here" },
  { "zm", desc = "Fold more" },
  { "zo", desc = "Open fold here" },
  { "zr", desc = "Fold less" },
  { "zv", desc = "View cursor line" },
  { "zx", desc = "Update folds and view cursor line" },
})

wk.add({
  {
    mode = { "o" },
    { "aC", desc = "Outer class" },
    { "ab", desc = "Outer block" },
    { "ac", desc = "Outer conditional" },
    { "ad", desc = "Outer comment" },
    { "af", desc = "Outer function" },
    { "al", desc = "Outer loop" },
    { "am", desc = "Outer call" },
    { "as", desc = "Outer statement" },
    { "iC", desc = "Inner class" },
    { "ib", desc = "Inner block" },
    { "ic", desc = "Inner conditional" },
    { "if", desc = "Inner function" },
    { "il", desc = "Inner loop" },
    { "im", desc = "Inner call" },
    { "is", desc = "Inner statement" },
  },
})

-- vim.api.nvim_set_keymap('', '<C-h>', '<C-w>h', {noremap = true, silent = false})
-- vim.api.nvim_set_keymap('', '<C-j>', '<C-w>j', {noremap = true, silent = false})
-- vim.api.nvim_set_keymap('', '<C-k>', '<C-w>k', {noremap = true, silent = false})
-- vim.api.nvim_set_keymap('', '<C-l>', '<C-w>l', {noremap = true, silent = false})


-- vim.api.nvim_set_keymap('n', '<C-n>', ':NvimTreeFindFile<CR>', {noremap = true, silent = false})
-- vim.api.nvim_set_keymap('n', '<leader>tt', ':NvimTreeToggle<CR>', {noremap = true, silent = false})
-- vim.api.nvim_set_keymap('n', '<leader>tr', ':NvimTreeRefresh<CR>', {noremap = true, silent = false})


-- vim.api.nvim_set_keymap('n', '<space>,', ':lua vim.lsp.diagnostic.goto_prev()<CR>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<space>;', ':lua vim.lsp.diagnostic.goto_next()<CR>', {noremap = true, silent = true})

-- vim.api.nvim_set_keymap('n', 'K', ':lua vim.lsp.buf.hover()<CR>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<space>m', ':lua vim.lsp.buf.rename()<CR>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<space>f', ':lua vim.lsp.buf.formatting()<CR>', {noremap = true, silent = true})

-- vim.api.nvim_set_keymap('n', '<space>a', ':lua vim.lsp.diagnostic.code_action()<CR>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', 'gd', ':lua vim.lsp.buf.definition()<CR>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', 'gD', ":lua require('telescope.builtin').lsp_implementations()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<space>r', ':lua vim.lsp.buf.references()<CR>', {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<space>s', ':lua vim.lsp.buf.document_symbol()<CR>', {noremap = true, silent = true})

-- telescope for word under the cursor
-- vim.api.nvim_set_keymap('n', '<leader>r', ":lua require('telescope.builtin').lsp_references()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>R', ":lua require('telescope.builtin').grep_string()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>a', ":lua require('telescope.builtin').lsp_code_actions()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', 'gd', ":lua require('telescope.builtin').lsp_definitions()<CR>", {noremap = true, silent = true})

-- list LSP things
-- vim.api.nvim_set_keymap('n', '<leader>s', ":lua require('telescope.builtin').lsp_document_symbols()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>S', ":lua require('telescope.builtin').lsp_workspace_symbols()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>d', ":lua require('telescope.builtin').lsp_document_diagnostics()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>D', ":lua require('telescope.builtin').lsp_workspace_diagnostics()<CR>", {noremap = true, silent = true})

-- quick access to: search in buffer / pick a file / search projectwide
-- vim.api.nvim_set_keymap('n', '<C-p>', ":lua require('plugins.telescope').project_files()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>/', ":lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>?', ":lua require('telescope.builtin').live_grep()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<C-p>', ":lua require('telescope.builtin').git_files()<CR>", {noremap = true, silent = true})

-- telescope for other stuff
-- vim.api.nvim_set_keymap('n', '<leader>b', ":lua require('telescope.builtin').buffers()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>fh', ":lua require('telescope.builtin').help_tags()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>fq', ":lua require('telescope.builtin').quickfix()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>fr', ":lua require('telescope.builtin').registers()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>fk', ":lua require('telescope.builtin').keymaps()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>fm', ":lua require('telescope.builtin').marks()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>ft', ":lua require('telescope.builtin').treesitter()<CR>", {noremap = true, silent = true})

-- telescope for git
-- vim.api.nvim_set_keymap('n', '<leader>c', ":lua require('plugins.telescope').my_git_commits()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>g', ":lua require('plugins.telescope').my_git_status()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>b', ":lua require('plugins.telescope').my_git_bcommits()<CR>", {noremap = true, silent = true})

-- vim.api.nvim_set_keymap('n', '<leader>p', ":lua require('telescope').extensions.project.project{}<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>n', ":lua require('plugins.telescope').my_note()<CR>", {noremap = true, silent = true})
-- vim.api.nvim_set_keymap('n', '<leader>nn', ":e ~/Note/", {noremap = true, silent = false})
-- vim.api.nvim_set_keymap('n', '<leader>gc', ':Octo issue create<CR>', {noremap = true, silent = false})
-- vim.api.nvim_set_keymap('n', '<leader>i', ':Octo issue list<CR>', {noremap = true, silent = false})

-- vim.api.nvim_set_keymap("n", "<leader>xx", "<cmd>Trouble<cr>",
--   {silent = true, noremap = true}
-- )
-- vim.api.nvim_set_keymap("n", "<leader>xw", "<cmd>Trouble lsp_workspace_diagnostics<cr>",
--   {silent = true, noremap = true}
-- )
-- vim.api.nvim_set_keymap("n", "<leader>xd", "<cmd>Trouble lsp_document_diagnostics<cr>",
--   {silent = true, noremap = true}
-- )
-- vim.api.nvim_set_keymap("n", "<leader>xl", "<cmd>Trouble loclist<cr>",
--   {silent = true, noremap = true}
-- )
-- vim.api.nvim_set_keymap("n", "<leader>xq", "<cmd>Trouble quickfix<cr>",
--   {silent = true, noremap = true}
-- )
-- vim.api.nvim_set_keymap("n", "gR", "<cmd>Trouble lsp_references<cr>",
--   {silent = true, noremap = true}
-- )
