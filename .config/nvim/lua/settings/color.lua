-- if vim.fn.has("termguicolors") == 1 then
--   vim.go.t_8f = "[[38;2;%lu;%lu;%lum"
--   vim.go.t_8b = "[[48;2;%lu;%lu;%lum"
-- end

vim.opt.termguicolors = true

-- vim.opt.background = "dark"
-- vim.cmd 'colorscheme breezy'
-- require('zephyr')
-- vim.cmd 'colorscheme kanagawa'
-- vim.cmd 'hi Normal'
vim.cmd 'hi Normal guibg=NONE guifg=#ffffff gui=NONE'

-- vim.api.nvim_set_hl(0, 'Normal', {bg = "#4E4E4E"})
