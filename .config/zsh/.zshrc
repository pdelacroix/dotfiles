#!/usr/bin/env zsh

# from https://insanet.eu/post/supercharge-your-terminal/

# Binary dependencies: 
# zoxide - no more cd
# exa - vastly better ls
# fzf - absolute terminal gamechanger
# ripgrep - faster and better grep
# nvim - better vim
# tldr - super-condensed man
# pkgfile - command-not-found - arch-linux-specific: automatically search the pkgfile database when entering an unrecognized command
# run (sudo) `pkgfile -u` beforehand to sync package database

# git clone --depth=1 https://github.com/mattmc3/antidote.git ${ZDOTDIR:-~}/.antidote
source ${ZDOTDIR:-~}/.antidote/antidote.zsh
antidote load

# if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#   source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
#
# fi
# [[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh

export LANG=en_US.UTF-8

export EDITOR='vim'
export DIFFPROG='nvim -d'
export MANPAGER='nvim +Man!'
export PAGER="less"

# export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow -g "!{.git,node_modules}/*" 2> /dev/null'
export FZF_DEFAULT_COMMAND='rg --no-messages --files --no-ignore --hidden --follow --glob "!.git/*"'
export FZF_DEFAULT_OPTS="--no-separator --layout=reverse --inline-info"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="cd ~/; bfs -type d -nohidden | sed s/^\./~/"

export ERL_AFLAGS="-kernel shell_history enabled"

# zoxide directory preview options
export _ZO_FZF_OPTS="--no-sort --keep-right --height=50% --info=inline --layout=reverse --exit-0 --select-1 --bind=ctrl-z:ignore --preview='\command exa --long --all {2..}' --preview-window=right"

#history
HISTFILE="${ZDOTDIR:-$HOME}/.zsh_history"       # The path to the history file.
HISTSIZE=1000000000              # The maximum number of events to save in the internal history.
SAVEHIST=1000000000              # The maximum number of events to save in the history file.

setopt BANG_HIST                 # Treat the '!' character specially during expansion.
setopt EXTENDED_HISTORY          # Write the history file in the ':start:elapsed;command' format.
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt SHARE_HISTORY             # Share history between all sessions.
setopt HIST_EXPIRE_DUPS_FIRST    # Expire duplicate entries first when trimming history.
setopt HIST_IGNORE_DUPS          # Don't record an entry that was just recorded again.
setopt HIST_IGNORE_ALL_DUPS      # Delete old recorded entry if new entry is a duplicate.
setopt HIST_FIND_NO_DUPS         # Do not display a line previously found.
setopt HIST_IGNORE_SPACE         # Don't record an entry starting with a space.
setopt HIST_SAVE_NO_DUPS         # Don't write duplicate entries in the history file.
setopt HIST_REDUCE_BLANKS        # Remove superfluous blanks before recording entry.
setopt HIST_VERIFY               # Do not execute immediately upon history expansion.

unsetopt beep

# set list-colors to enable filename colorizing
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'exa -1 --color=always $realpath'
zstyle ':fzf-tab:complete:systemctl-*:*' fzf-preview 'SYSTEMD_COLORS=1 systemctl status $word'
zstyle ':fzf-tab:complete:tldr:argument-1' fzf-preview 'tldr --color always $word'
# switch group using `,` and `.`
zstyle ':fzf-tab:*' switch-group ',' '.'
# exclude .. and . from completion
zstyle ':completion:*' special-dirs false
# show hidden files in completion
setopt glob_dots

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets cursor root line)
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#4e4e4e"
ZSH_HIGHLIGHT_STYLES[cursor]='bg=#4e4e4e'

# emacs zle keys
# https://zsh.sourceforge.io/Guide/zshguide04.html
bindkey -e
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}" end-of-line
bindkey "${terminfo[kdch1]}" delete-char
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

##ALIASES
alias ls=eza
alias ll='eza --long --all --group-directories-first --git'
alias cat=bat
alias tmux='TERM=xterm-256color tmux'
alias vim=nvim

# arch-specific dependencies
# source /usr/share/doc/pkgfile/command-not-found.zsh
# source /usr/share/fzf/key-bindings.zsh
# source /usr/share/fzf/completion.zsh

[ -f /usr/share/fzf/shell/key-bindings.zsh ] && . /usr/share/fzf/shell/key-bindings.zsh
[ -f /usr/share/fzf/shell/completion.zsh ] && . /usr/share/fzf/shell/completion.zsh

# [ -d /etc/zsh_completion.d ] && fpath=(/etc/zsh_completion.d $fpath)
[ -f /etc/zsh_completion.d/fzf-key-bindings ] && . /etc/zsh_completion.d/fzf-key-bindings

# enable zsh completion
autoload -Uz compinit && compinit

# source scripts from ~/bin
# export PATH="$HOME/bin:$PATH"

export PATH="$PATH:$HOME/.local/bin"

export PATH="$PATH:$HOME/node_modules/.bin"
export PATH="$PATH:$HOME/.yarn/bin"
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="$PATH:$HOME/.mix/escripts"
export PATH="$HOME/.npm/bin:$PATH"

export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

# TODO: source if exists
# . /opt/asdf-vm/asdf.sh
# . /usr/share/asdf/asdf.sh
# . $HOME/.asdf/asdf.sh
# . $HOME/.asdf/completions/asdf.bash
# TODO: add completions
. $HOME/.asdf/asdf.sh

# . /etc/profile.d/fzf-extras.zsh

# opam configuration
test -r /home/pierre/.opam/opam-init/init.zsh && . /home/pierre/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# kubctl
if [ /usr/bin/kubectl ]; then source <(kubectl completion zsh); fi

eval "$(starship init zsh)"

# source /home/pierre/.config/broot/launcher/bash/br

export MIX_HOME="$HOME/.mix"
export MIX_ARCHIVES="$MIX_HOME/archives"

export ANSIBLE_NOCOWS=1

. $HOME/dev/git-subrepo/.rc

eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
# export PATH="$PATH:$HOME/.rvm/bin"
