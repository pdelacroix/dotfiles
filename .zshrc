#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

#export PATH="/nfs/zfs-student-3/users/2013/pde-lacr/.local/usr/local/bin:/nfs/zfs-student-3/users/2013/pde-lacr/.brew/opt/nginx/sbin:/nfs/zfs-student-3/users/2013/pde-lacr/.brew/opt/ruby/bin:/nfs/zfs-student-3/users/2013/pde-lacr/.brew/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/opt/X11/bin:/usr/texbin:/nfs/zfs-student-3/users/2013/pde-lacr/.brew/bin"
#export HOME="/Volumes/Data/nfs/zfs-student-3/users/2013/pde-lacr"

# Customize to your needs...

alias tmux='TERM=xterm-256color tmux'
# eval $(thefuck --alias)
alias vim=nvim

export PAGER="less"
export EDITOR="vim"

[ -f /usr/share/fzf/key-bindings.zsh ] && . /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/fzf/completion.zsh ] && . /usr/share/fzf/completion.zsh

# [ -d /etc/zsh_completion.d ] && fpath=(/etc/zsh_completion.d $fpath)
[ -f /etc/zsh_completion.d/fzf-key-bindings ] && . /etc/zsh_completion.d/fzf-key-bindings

export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow -g "!{.git,node_modules}/*" 2> /dev/null'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="cd ~/; bfs -type d -nohidden | sed s/^\./~/"

export ERL_AFLAGS="-kernel shell_history enabled"

export PATH="$PATH:$HOME/.local/bin"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
# export PATH="$PATH:$HOME/.rvm/bin"

export PATH="$PATH:$HOME/node_modules/.bin"
export PATH="$PATH:$HOME/.yarn/bin"
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="$PATH:$HOME/.mix/escripts"
export PATH="$HOME/.npm/bin:$PATH"

# TODO: source if exists
# . /opt/asdf-vm/asdf.sh
# . /usr/share/asdf/asdf.sh
# . $HOME/.asdf/asdf.sh
# . $HOME/.asdf/completions/asdf.bash
# TODO: add completions
. $HOME/.asdf/asdf.sh

# . /etc/profile.d/fzf-extras.zsh

# opam configuration
test -r /home/pierre/.opam/opam-init/init.zsh && . /home/pierre/.opam/opam-init/init.zsh > /dev/null 2> /dev/null || true

# kubctl
if [ /usr/bin/kubectl ]; then source <(kubectl completion zsh); fi

eval "$(starship init zsh)"

# source /home/pierre/.config/broot/launcher/bash/br

alias ls=eza
alias cat=bat

export MIX_HOME="$HOME/.mix"
export MIX_ARCHIVES="$MIX_HOME/archives"

export ANSIBLE_NOCOWS=1

. $HOME/dev/git-subrepo/.rc

eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
# export PATH="$PATH:$HOME/.rvm/bin"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
# export PATH="$PATH:$HOME/.rvm/bin"
